package co.com.adsalomo.encuestas.servicios;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Inicia la aplicación
 *
 */
@SpringBootApplication
public class MainApp 
{
    public static void main( String[] args )
    {
        SpringApplication.run(MainApp.class, args);
    }
    
}

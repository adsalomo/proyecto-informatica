package co.com.adsalomo.encuestas.servicios.dao.catalogo.iface;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import co.com.adsalomo.encuestas.servicios.dao.model.Encuesta;

/**
 * Repositorio Recuerso Encuesta
 * @author alopez
 *
 */
public interface EncuestaRepository extends JpaRepository<Encuesta, Integer> {

	List<Encuesta> findByActivo(Boolean activo);
	
	Encuesta findByEncuestaId(Integer encuestaId);
}

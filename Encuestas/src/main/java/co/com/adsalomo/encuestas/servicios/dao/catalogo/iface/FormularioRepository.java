package co.com.adsalomo.encuestas.servicios.dao.catalogo.iface;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import co.com.adsalomo.encuestas.servicios.dao.model.Formulario;

/**
 * Repositorio recurso Formulario
 * @author alopez
 *
 */
public interface FormularioRepository extends JpaRepository<Formulario, Integer> {

	@Query("SELECT f FROM Formulario f WHERE f.encuesta.encuestaId = ?1 AND f.fechaFin IS NULL")
	Formulario findByEncuestaId(Integer encuestaId);
}

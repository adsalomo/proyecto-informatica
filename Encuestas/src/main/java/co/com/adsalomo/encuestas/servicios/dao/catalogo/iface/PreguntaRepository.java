package co.com.adsalomo.encuestas.servicios.dao.catalogo.iface;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import co.com.adsalomo.encuestas.servicios.dao.model.Pregunta;

/**
 * Repositorio recurso pregunta
 * @author alopez
 *
 */
public interface PreguntaRepository extends JpaRepository<Pregunta, Integer> {

	@Query("SELECT p FROM Pregunta p WHERE p.formulario.formularioId = ?1")
	List<Pregunta> findByFormularioId(Integer formularioId);
}

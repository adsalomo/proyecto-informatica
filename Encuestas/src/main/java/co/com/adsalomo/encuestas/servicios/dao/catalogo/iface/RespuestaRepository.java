package co.com.adsalomo.encuestas.servicios.dao.catalogo.iface;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import co.com.adsalomo.encuestas.servicios.dao.model.Respuesta;

public interface RespuestaRepository extends JpaRepository<Respuesta, Integer> {

	@Query("SELECT r FROM Respuesta r WHERE r.pregunta.preguntaId = ?1")
	List<Respuesta> findByPreguntaId(Integer preguntaId);
}

package co.com.adsalomo.encuestas.servicios.dao.catalogo.iface;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import co.com.adsalomo.encuestas.servicios.dao.model.TipoDato;

/**
 * Repositorio recuerso Tipo dato
 * @author alopez
 *
 */
public interface TipoDatoRepository extends JpaRepository<TipoDato, Integer> {

	/**
	 * Obtiene todos por activo
	 * @param activo
	 * @return
	 */
	List<TipoDato> findByActivo(Boolean activo);
}

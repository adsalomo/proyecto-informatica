package co.com.adsalomo.encuestas.servicios.dao.catalogo.iface;

import org.springframework.data.jpa.repository.JpaRepository;

import co.com.adsalomo.encuestas.servicios.dao.model.TipoPregunta;

/**
 * Repositorio recuerso Tipo pregunta
 * @author alopez
 *
 */
public interface TipoPreguntaRepository extends JpaRepository<TipoPregunta, Integer> {

}

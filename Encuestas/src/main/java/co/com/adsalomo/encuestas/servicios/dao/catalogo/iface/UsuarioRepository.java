package co.com.adsalomo.encuestas.servicios.dao.catalogo.iface;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import co.com.adsalomo.encuestas.servicios.dao.model.Usuario;

/**
 * Repositorio recurso Usuario
 * @author alopez
 *
 */
public interface UsuarioRepository extends JpaRepository<Usuario, Integer>{

	List<Usuario> findByActivo(Boolean activo);
}

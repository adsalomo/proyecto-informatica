package co.com.adsalomo.encuestas.servicios.dao.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;


/**
 * The persistent class for the formulario database table.
 * 
 */
@Entity
@NamedQuery(name="Formulario.findAll", query="SELECT f FROM Formulario f")
public class Formulario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="formulario_id")
	private Integer formularioId;

	@Column(name="fecha_fin")
	private Date fechaFin;

	@Column(name="fecha_inicio")
	private Date fechaInicio;

	@Column(name="fecha_modifica")
	private Date fechaModifica;

	private String nombre;

	@Column(name="usuario_modifica")
	private Integer usuarioModifica;

	//bi-directional many-to-one association to Encuesta
	@ManyToOne
	@JoinColumn(name="encuesta_id")
	private Encuesta encuesta;

	//bi-directional many-to-one association to TipoPregunta
	@ManyToOne
	@JoinColumn(name="tipo_pregunta_id")
	private TipoPregunta tipoPregunta;

	public Formulario() {
	}

	public Integer getFormularioId() {
		return this.formularioId;
	}

	public void setFormularioId(Integer formularioId) {
		this.formularioId = formularioId;
	}

	public Date getFechaFin() {
		return this.fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public Date getFechaInicio() {
		return this.fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaModifica() {
		return this.fechaModifica;
	}

	public void setFechaModifica(Date fechaModifica) {
		this.fechaModifica = fechaModifica;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getUsuarioModifica() {
		return this.usuarioModifica;
	}

	public void setUsuarioModifica(Integer usuarioModifica) {
		this.usuarioModifica = usuarioModifica;
	}

	public Encuesta getEncuesta() {
		return this.encuesta;
	}

	public void setEncuesta(Encuesta encuesta) {
		this.encuesta = encuesta;
	}

	public TipoPregunta getTipoPregunta() {
		return this.tipoPregunta;
	}

	public void setTipoPregunta(TipoPregunta tipoPregunta) {
		this.tipoPregunta = tipoPregunta;
	}

}
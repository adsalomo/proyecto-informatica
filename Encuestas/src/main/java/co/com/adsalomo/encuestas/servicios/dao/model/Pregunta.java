package co.com.adsalomo.encuestas.servicios.dao.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;


/**
 * The persistent class for the pregunta database table.
 * 
 */
@Entity
@NamedQuery(name="Pregunta.findAll", query="SELECT p FROM Pregunta p")
public class Pregunta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pregunta_id")
	private Integer preguntaId;

	private Boolean activo;

	private String etiqueta;

	@Column(name="fecha_modifica")
	private Date fechaModifica;

	private Integer longitud;

	private Boolean requerido;

	@Column(name="usuario_modifica")
	private Integer usuarioModifica;

	private String valor;

	//bi-directional many-to-one association to Formulario
	@ManyToOne
	@JoinColumn(name="formulario_id")
	private Formulario formulario;

	//bi-directional many-to-one association to TipoDato
	@ManyToOne
	@JoinColumn(name="tipo_dato_id")
	private TipoDato tipoDato;

	public Pregunta() {
	}

	public Integer getPreguntaId() {
		return this.preguntaId;
	}

	public void setPreguntaId(Integer preguntaId) {
		this.preguntaId = preguntaId;
	}

	public Boolean getActivo() {
		return this.activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public String getEtiqueta() {
		return this.etiqueta;
	}

	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

	public Date getFechaModifica() {
		return this.fechaModifica;
	}

	public void setFechaModifica(Date fechaModifica) {
		this.fechaModifica = fechaModifica;
	}

	public Integer getLongitud() {
		return this.longitud;
	}

	public void setLongitud(Integer longitud) {
		this.longitud = longitud;
	}

	public Boolean getRequerido() {
		return this.requerido;
	}

	public void setRequerido(Boolean requerido) {
		this.requerido = requerido;
	}

	public Integer getUsuarioModifica() {
		return this.usuarioModifica;
	}

	public void setUsuarioModifica(Integer usuarioModifica) {
		this.usuarioModifica = usuarioModifica;
	}

	public String getValor() {
		return this.valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public Formulario getFormulario() {
		return this.formulario;
	}

	public void setFormulario(Formulario formulario) {
		this.formulario = formulario;
	}

	public TipoDato getTipoDato() {
		return this.tipoDato;
	}

	public void setTipoDato(TipoDato tipoDato) {
		this.tipoDato = tipoDato;
	}

}
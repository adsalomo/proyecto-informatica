package co.com.adsalomo.encuestas.servicios.dao.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;


/**
 * The persistent class for the respuesta database table.
 * 
 */
@Entity
@NamedQuery(name="Respuesta.findAll", query="SELECT r FROM Respuesta r")
public class Respuesta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="respuesta_id")
	private Integer respuestaId;

	@Column(name="fecha_proceso")
	private Date fechaProceso;

	@Column(name="usuario_modifica")
	private Integer usuarioModifica;

	private String valor;

	//bi-directional many-to-one association to Pregunta
	@ManyToOne
	@JoinColumn(name="pregunta_id")
	private Pregunta pregunta;

	public Respuesta() {
	}

	public Integer getRespuestaId() {
		return this.respuestaId;
	}

	public void setRespuestaId(Integer respuestaId) {
		this.respuestaId = respuestaId;
	}

	public Date getFechaProceso() {
		return this.fechaProceso;
	}

	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso;
	}

	public Integer getUsuarioModifica() {
		return this.usuarioModifica;
	}

	public void setUsuarioModifica(Integer usuarioModifica) {
		this.usuarioModifica = usuarioModifica;
	}

	public String getValor() {
		return this.valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public Pregunta getPregunta() {
		return this.pregunta;
	}

	public void setPregunta(Pregunta pregunta) {
		this.pregunta = pregunta;
	}

}
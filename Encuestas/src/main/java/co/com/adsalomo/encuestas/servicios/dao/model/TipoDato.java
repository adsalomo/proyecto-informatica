package co.com.adsalomo.encuestas.servicios.dao.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tipo_dato database table.
 * 
 */
@Entity
@Table(name="tipo_dato")
@NamedQuery(name="TipoDato.findAll", query="SELECT t FROM TipoDato t")
public class TipoDato implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="tipo_dato_id")
	private Integer tipoDatoId;

	private Boolean activo;

	private String nombre;

	//bi-directional many-to-one association to Pregunta
	@OneToMany(mappedBy="tipoDato", fetch=FetchType.EAGER)
	private List<Pregunta> preguntas;

	public TipoDato() {
	}
	
	public TipoDato(Integer tipoDatoId) {
		this.tipoDatoId = tipoDatoId;
	}

	public Integer getTipoDatoId() {
		return this.tipoDatoId;
	}

	public void setTipoDatoId(Integer tipoDatoId) {
		this.tipoDatoId = tipoDatoId;
	}

	public Boolean getActivo() {
		return this.activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Pregunta> getPreguntas() {
		return this.preguntas;
	}

	public void setPreguntas(List<Pregunta> preguntas) {
		this.preguntas = preguntas;
	}

	public Pregunta addPregunta(Pregunta pregunta) {
		getPreguntas().add(pregunta);
		pregunta.setTipoDato(this);

		return pregunta;
	}

	public Pregunta removePregunta(Pregunta pregunta) {
		getPreguntas().remove(pregunta);
		pregunta.setTipoDato(null);

		return pregunta;
	}

}
package co.com.adsalomo.encuestas.servicios.dao.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tipo_pregunta database table.
 * 
 */
@Entity
@Table(name="tipo_pregunta")
@NamedQuery(name="TipoPregunta.findAll", query="SELECT t FROM TipoPregunta t")
public class TipoPregunta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="tipo_pregunta_id")
	private Integer tipoPreguntaId;

	private Boolean activo;

	private String nombre;

	//bi-directional many-to-one association to Formulario
	@OneToMany(mappedBy="tipoPregunta", fetch=FetchType.EAGER)
	private List<Formulario> formularios;

	public TipoPregunta() {
	}
	
	public TipoPregunta(Integer tipoPreguntaId) {
		this.tipoPreguntaId = tipoPreguntaId;
	}

	public Integer getTipoPreguntaId() {
		return this.tipoPreguntaId;
	}

	public void setTipoPreguntaId(Integer tipoPreguntaId) {
		this.tipoPreguntaId = tipoPreguntaId;
	}

	public Boolean getActivo() {
		return this.activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Formulario> getFormularios() {
		return this.formularios;
	}

	public void setFormularios(List<Formulario> formularios) {
		this.formularios = formularios;
	}

	public Formulario addFormulario(Formulario formulario) {
		getFormularios().add(formulario);
		formulario.setTipoPregunta(this);

		return formulario;
	}

	public Formulario removeFormulario(Formulario formulario) {
		getFormularios().remove(formulario);
		formulario.setTipoPregunta(null);

		return formulario;
	}

}
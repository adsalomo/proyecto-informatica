package co.com.adsalomo.encuestas.servicios.endpoint.catalogo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.adsalomo.encuestas.servicios.endpoint.catalogo.dto.CatalogoRespuestaDto;
import co.com.adsalomo.encuestas.servicios.endpoint.catalogo.dto.EncuestaDto;
import co.com.adsalomo.encuestas.servicios.service.catalogo.iface.EncuestaService;

@RestController
public class EncuestaRestEndPoint {
	private static final String MODULO = "catalogo";
	private static final String ENTIDAD = "encuesta";

	@Autowired
	private EncuestaService encuestaService;

	/**
	 * Servicio listar usuarios
	 * 
	 * @param activo
	 * @return
	 */
	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/" + MODULO + "/" + ENTIDAD
			+ "/{activo}", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
	public ResponseEntity<List<EncuestaDto>> listarPorActivo(@PathVariable(value = "activo") Boolean activo) {
		ResponseEntity<List<EncuestaDto>> responseEntity;
		try {
			List<EncuestaDto> encuestaDtos = encuestaService.listarPorActivo(activo);
			responseEntity = new ResponseEntity<>(encuestaDtos, HttpStatus.OK);
		} catch (Exception ex) {
			ex.printStackTrace();
			responseEntity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return responseEntity;
	}

	/**
	 * Obtiene encuesta por id
	 * 
	 * @param encuestaId
	 * @return
	 */
	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/" + MODULO + "/" + ENTIDAD
			+ "/obtenerXEncuestaId/{encuestaId}", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
	public ResponseEntity<EncuestaDto> listarPorEncuestaId(@PathVariable(value = "encuestaId") Integer encuestaId) {
		ResponseEntity<EncuestaDto> responseEntity;
		try {
			EncuestaDto encuestaDto = encuestaService.listarPorEncuestaId(encuestaId);
			responseEntity = new ResponseEntity<>(encuestaDto, HttpStatus.OK);
		} catch (Exception ex) {
			ex.printStackTrace();
			responseEntity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return responseEntity;
	}
	
	/**
	 * Guarda encuesta
	 * @param encuestaDto
	 * @return
	 */
	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/" + MODULO + "/" + ENTIDAD + "", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	public ResponseEntity<CatalogoRespuestaDto> guardar(@RequestBody EncuestaDto encuestaDto) {
		ResponseEntity<CatalogoRespuestaDto> responseEntity;
		try {
			CatalogoRespuestaDto respuestaDto = encuestaService.guardar(encuestaDto);
			responseEntity = new ResponseEntity<>(respuestaDto, HttpStatus.OK);
		} catch (Exception ex) {
			ex.printStackTrace();
			responseEntity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return responseEntity;
	}
	
	/**
	 * Obtiene los resultados de la encuesta
	 * @param encuestaId
	 * @return
	 */
	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/" + MODULO + "/" + ENTIDAD
			+ "/obtenerResultados/{encuestaId}", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
	public ResponseEntity<EncuestaDto> obtenerResultados(@PathVariable(value = "encuestaId") Integer encuestaId) {
		ResponseEntity<EncuestaDto> responseEntity;
		try {
			EncuestaDto encuestaDto = encuestaService.obtenerResultados(encuestaId);
			responseEntity = new ResponseEntity<>(encuestaDto, HttpStatus.OK);
		} catch (Exception ex) {
			ex.printStackTrace();
			responseEntity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return responseEntity;
	}
}

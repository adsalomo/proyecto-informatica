package co.com.adsalomo.encuestas.servicios.endpoint.catalogo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.adsalomo.encuestas.servicios.endpoint.catalogo.dto.CatalogoRespuestaDto;
import co.com.adsalomo.encuestas.servicios.endpoint.catalogo.dto.RespuestaDto;
import co.com.adsalomo.encuestas.servicios.service.catalogo.iface.RespuestaService;

@RestController
public class RespuestaRestEndPoint {
	private static final String MODULO = "catalogo";
    private static final String ENTIDAD = "respuesta";
    
    @Autowired private RespuestaService respuestaService;
    
    /**
     * Servicio listar usuarios
     * @param activo
     * @return
     */
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/" + MODULO + "/" + ENTIDAD + "", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public ResponseEntity<CatalogoRespuestaDto> guardar(@RequestBody List<RespuestaDto> respuestaDtos){
    	ResponseEntity<CatalogoRespuestaDto> responseEntity;
    	try {
    		CatalogoRespuestaDto respuestaDto = respuestaService.guardar(respuestaDtos);
    		responseEntity = new ResponseEntity<>(respuestaDto, HttpStatus.OK);
    	} catch (Exception ex) {
    		ex.printStackTrace();
    		responseEntity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    	}
      
    	return responseEntity;
    }
}

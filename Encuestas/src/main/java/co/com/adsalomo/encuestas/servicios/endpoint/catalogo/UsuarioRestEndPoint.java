package co.com.adsalomo.encuestas.servicios.endpoint.catalogo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.adsalomo.encuestas.servicios.endpoint.catalogo.dto.UsuarioDto;
import co.com.adsalomo.encuestas.servicios.service.catalogo.iface.UsuarioService;

/**
 * Controlador usuario
 * @author alopez
 *
 */
@RestController
public class UsuarioRestEndPoint {
	private static final String MODULO = "catalogo";
    private static final String ENTIDAD = "usuario";
    
    @Autowired private UsuarioService usuarioService;
    
    /**
     * Servicio listar usuarios
     * @param activo
     * @return
     */
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/" + MODULO + "/" + ENTIDAD + "/{activo}", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
    public ResponseEntity<List<UsuarioDto>> listarPorActivo(@PathVariable(value = "activo") Boolean activo){
    	ResponseEntity<List<UsuarioDto>> responseEntity;
    	try {
    		List<UsuarioDto> usuarioDtos = usuarioService.listarPorActivo(activo);
    		responseEntity = new ResponseEntity<>(usuarioDtos, HttpStatus.OK);
    	} catch (Exception ex) {
    		ex.printStackTrace();
    		responseEntity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    	}
      
    	return responseEntity;
    }
	
}

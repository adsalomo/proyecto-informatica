package co.com.adsalomo.encuestas.servicios.endpoint.catalogo.dto;

public class CatalogoRespuestaDto {
	private boolean estado;
	private String respuesta;
	
	public CatalogoRespuestaDto() {
		
	}

	/**
	 * @return the estado
	 */
	public boolean isEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	/**
	 * @return the respuesta
	 */
	public String getRespuesta() {
		return respuesta;
	}

	/**
	 * @param respuesta the respuesta to set
	 */
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	
	
}

package co.com.adsalomo.encuestas.servicios.endpoint.catalogo.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Dto encuesta
 * @author alopez
 *
 */
public class EncuestaDto implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer encuestaId;
	private Boolean activo;
	private Date fechaCrea;
	private Date fechaModifica;
	private String nombre;
	private String objetivo;
	private Integer usuarioModifica;
	private Integer usuarioCrea;
	private FormularioDto formulario;

	public EncuestaDto() {
	}

	/**
	 * @return the ecuestaId
	 */
	public Integer getEncuestaId() {
		return encuestaId;
	}

	/**
	 * @param ecuestaId the ecuestaId to set
	 */
	public void setEncuestaId(Integer encuestaId) {
		this.encuestaId = encuestaId;
	}

	/**
	 * @return the activo
	 */
	public Boolean getActivo() {
		return activo;
	}

	/**
	 * @param activo the activo to set
	 */
	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	/**
	 * @return the fechaCrea
	 */
	public Date getFechaCrea() {
		return fechaCrea;
	}

	/**
	 * @param fechaCrea the fechaCrea to set
	 */
	public void setFechaCrea(Date fechaCrea) {
		this.fechaCrea = fechaCrea;
	}

	/**
	 * @return the fechaModifica
	 */
	public Date getFechaModifica() {
		return fechaModifica;
	}

	/**
	 * @param fechaModifica the fechaModifica to set
	 */
	public void setFechaModifica(Date fechaModifica) {
		this.fechaModifica = fechaModifica;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the objetivo
	 */
	public String getObjetivo() {
		return objetivo;
	}

	/**
	 * @param objetivo the objetivo to set
	 */
	public void setObjetivo(String objetivo) {
		this.objetivo = objetivo;
	}

	/**
	 * @return the usuarioModifica
	 */
	public Integer getUsuarioModifica() {
		return usuarioModifica;
	}

	/**
	 * @param usuarioModifica the usuarioModifica to set
	 */
	public void setUsuarioModifica(Integer usuarioModifica) {
		this.usuarioModifica = usuarioModifica;
	}

	/**
	 * @return the usuarioCrea
	 */
	public Integer getUsuarioCrea() {
		return usuarioCrea;
	}

	/**
	 * @param usuarioCrea the usuarioCrea to set
	 */
	public void setUsuarioCrea(Integer usuarioCrea) {
		this.usuarioCrea = usuarioCrea;
	}

	/**
	 * @return the formulario
	 */
	public FormularioDto getFormulario() {
		return formulario;
	}

	/**
	 * @param formulario the formulario to set
	 */
	public void setFormulario(FormularioDto formulario) {
		this.formulario = formulario;
	}



}
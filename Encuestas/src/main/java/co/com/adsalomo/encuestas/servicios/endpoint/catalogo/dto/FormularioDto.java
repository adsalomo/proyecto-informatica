package co.com.adsalomo.encuestas.servicios.endpoint.catalogo.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Dto formulario
 * @author alopez
 *
 */
public class FormularioDto implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer formularioId;
	private Date fechaFin;
	private Date fechaInicio;
	private Date fechaModifica;
	private String nombre;
	private Integer usuarioModifica;
	private Integer encuestaId;
	private Integer tipoPreguntaId;
	private String nombreTipoPregunta;
	private List<PreguntaDto> preguntas;

	public FormularioDto() {
	}

	/**
	 * @return the formularioId
	 */
	public Integer getFormularioId() {
		return formularioId;
	}

	/**
	 * @param formularioId the formularioId to set
	 */
	public void setFormularioId(Integer formularioId) {
		this.formularioId = formularioId;
	}

	/**
	 * @return the fechaFin
	 */
	public Date getFechaFin() {
		return fechaFin;
	}

	/**
	 * @param fechaFin the fechaFin to set
	 */
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	/**
	 * @return the fechaInicio
	 */
	public Date getFechaInicio() {
		return fechaInicio;
	}

	/**
	 * @param fechaInicio the fechaInicio to set
	 */
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	/**
	 * @return the fechaModifica
	 */
	public Date getFechaModifica() {
		return fechaModifica;
	}

	/**
	 * @param fechaModifica the fechaModifica to set
	 */
	public void setFechaModifica(Date fechaModifica) {
		this.fechaModifica = fechaModifica;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the usuarioModifica
	 */
	public Integer getUsuarioModifica() {
		return usuarioModifica;
	}

	/**
	 * @param usuarioModifica the usuarioModifica to set
	 */
	public void setUsuarioModifica(Integer usuarioModifica) {
		this.usuarioModifica = usuarioModifica;
	}

	/**
	 * @return the encuestaId
	 */
	public Integer getEncuestaId() {
		return encuestaId;
	}

	/**
	 * @param encuestaId the encuestaId to set
	 */
	public void setEncuestaId(Integer encuestaId) {
		this.encuestaId = encuestaId;
	}

	/**
	 * @return the tipoPreguntaId
	 */
	public Integer getTipoPreguntaId() {
		return tipoPreguntaId;
	}

	/**
	 * @param tipoPreguntaId the tipoPreguntaId to set
	 */
	public void setTipoPreguntaId(Integer tipoPreguntaId) {
		this.tipoPreguntaId = tipoPreguntaId;
	}

	/**
	 * @return the preguntas
	 */
	public List<PreguntaDto> getPreguntas() {
		return preguntas;
	}

	/**
	 * @param preguntas the preguntas to set
	 */
	public void setPreguntas(List<PreguntaDto> preguntas) {
		this.preguntas = preguntas;
	}

	/**
	 * @return the nombreTipoPregunta
	 */
	public String getNombreTipoPregunta() {
		return nombreTipoPregunta;
	}

	/**
	 * @param nombreTipoPregunta the nombreTipoPregunta to set
	 */
	public void setNombreTipoPregunta(String nombreTipoPregunta) {
		this.nombreTipoPregunta = nombreTipoPregunta;
	}

	
}
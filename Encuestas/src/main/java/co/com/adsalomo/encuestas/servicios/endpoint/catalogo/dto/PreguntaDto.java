package co.com.adsalomo.encuestas.servicios.endpoint.catalogo.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Dto pregunta
 * @author alopez
 *
 */
public class PreguntaDto implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer preguntaId;
	private Boolean activo;
	private String etiqueta;
	private Date fechaModifica;
	private Integer longitud;
	private Boolean requerido;
	private Integer usuarioModifica;
	private String valor;
	private Integer formularioId;
	private Integer tipoDatoId;
	private List<String> valores;
	private List<RespuestaDto> respuestas;
	private String etiquetaVal;

	public PreguntaDto() {
	}

	/**
	 * @return the preguntaId
	 */
	public Integer getPreguntaId() {
		return preguntaId;
	}

	/**
	 * @param preguntaId the preguntaId to set
	 */
	public void setPreguntaId(Integer preguntaId) {
		this.preguntaId = preguntaId;
	}

	/**
	 * @return the activo
	 */
	public Boolean getActivo() {
		return activo;
	}

	/**
	 * @param activo the activo to set
	 */
	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	/**
	 * @return the etiqueta
	 */
	public String getEtiqueta() {
		return etiqueta;
	}

	/**
	 * @param etiqueta the etiqueta to set
	 */
	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

	/**
	 * @return the fechaModifica
	 */
	public Date getFechaModifica() {
		return fechaModifica;
	}

	/**
	 * @param fechaModifica the fechaModifica to set
	 */
	public void setFechaModifica(Date fechaModifica) {
		this.fechaModifica = fechaModifica;
	}

	/**
	 * @return the logitud
	 */
	public Integer getLongitud() {
		return longitud;
	}

	/**
	 * @param logitud the logitud to set
	 */
	public void setLongitud(Integer longitud) {
		this.longitud = longitud;
	}

	/**
	 * @return the requerido
	 */
	public Boolean getRequerido() {
		return requerido;
	}

	/**
	 * @param requerido the requerido to set
	 */
	public void setRequerido(Boolean requerido) {
		this.requerido = requerido;
	}

	/**
	 * @return the usuarioModifica
	 */
	public Integer getUsuarioModifica() {
		return usuarioModifica;
	}

	/**
	 * @param usuarioModifica the usuarioModifica to set
	 */
	public void setUsuarioModifica(Integer usuarioModifica) {
		this.usuarioModifica = usuarioModifica;
	}

	/**
	 * @return the valor
	 */
	public String getValor() {
		return valor;
	}

	/**
	 * @param valor the valor to set
	 */
	public void setValor(String valor) {
		this.valor = valor;
	}

	/**
	 * @return the formularioId
	 */
	public Integer getFormularioId() {
		return formularioId;
	}

	/**
	 * @param formularioId the formularioId to set
	 */
	public void setFormularioId(Integer formularioId) {
		this.formularioId = formularioId;
	}

	/**
	 * @return the tipoDatoId
	 */
	public Integer getTipoDatoId() {
		return tipoDatoId;
	}

	/**
	 * @param tipoDatoId the tipoDatoId to set
	 */
	public void setTipoDatoId(Integer tipoDatoId) {
		this.tipoDatoId = tipoDatoId;
	}

	/**
	 * @return the valores
	 */
	public List<String> getValores() {
		return valores;
	}

	/**
	 * @param valores the valores to set
	 */
	public void setValores(List<String> valores) {
		this.valores = valores;
	}

	/**
	 * @return the respuestas
	 */
	public List<RespuestaDto> getRespuestas() {
		return respuestas;
	}

	/**
	 * @param respuestas the respuestas to set
	 */
	public void setRespuestas(List<RespuestaDto> respuestas) {
		this.respuestas = respuestas;
	}

	/**
	 * @return the etiquetaVal
	 */
	public String getEtiquetaVal() {
		return etiquetaVal;
	}

	/**
	 * @param etiquetaVal the etiquetaVal to set
	 */
	public void setEtiquetaVal(String etiquetaVal) {
		this.etiquetaVal = etiquetaVal;
	}

	
}
package co.com.adsalomo.encuestas.servicios.endpoint.catalogo.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the formulario database table.
 * 
 */

public class RespuestaDto implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer respuestaId;
	private Integer preguntaId;
	private String etiquetaPregunta;
	private String valor;
	private Date fechaProceso;
	private Integer usuarioModifica;

	public RespuestaDto() {
	}

	/**
	 * @return the respuestaId
	 */
	public Integer getRespuestaId() {
		return respuestaId;
	}

	/**
	 * @param respuestaId the respuestaId to set
	 */
	public void setRespuestaId(Integer respuestaId) {
		this.respuestaId = respuestaId;
	}

	/**
	 * @return the preguntaId
	 */
	public Integer getPreguntaId() {
		return preguntaId;
	}

	/**
	 * @param preguntaId the preguntaId to set
	 */
	public void setPreguntaId(Integer preguntaId) {
		this.preguntaId = preguntaId;
	}

	/**
	 * @return the valor
	 */
	public String getValor() {
		return valor;
	}

	/**
	 * @param valor the valor to set
	 */
	public void setValor(String valor) {
		this.valor = valor;
	}

	/**
	 * @return the fechaProceso
	 */
	public Date getFechaProceso() {
		return fechaProceso;
	}

	/**
	 * @param fechaProceso the fechaProceso to set
	 */
	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso;
	}

	/**
	 * @return the usuarioModifica
	 */
	public Integer getUsuarioModifica() {
		return usuarioModifica;
	}

	/**
	 * @param usuarioModifica the usuarioModifica to set
	 */
	public void setUsuarioModifica(Integer usuarioModifica) {
		this.usuarioModifica = usuarioModifica;
	}

	/**
	 * @return the etiquetaPregunta
	 */
	public String getEtiquetaPregunta() {
		return etiquetaPregunta;
	}

	/**
	 * @param etiquetaPregunta the etiquetaPregunta to set
	 */
	public void setEtiquetaPregunta(String etiquetaPregunta) {
		this.etiquetaPregunta = etiquetaPregunta;
	}

	

	
}
package co.com.adsalomo.encuestas.servicios.endpoint.catalogo.dto;

import java.io.Serializable;

/**
 * Dto Tipo Dato
 * @author alopez
 *
 */
public class TipoDatoDto implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer tipoDatoId;
	private Boolean activo;
	private String nombre;

	public TipoDatoDto() {
	}

	/**
	 * @return the tipoDatoId
	 */
	public Integer getTipoDatoId() {
		return tipoDatoId;
	}

	/**
	 * @param tipoDatoId the tipoDatoId to set
	 */
	public void setTipoDatoId(Integer tipoDatoId) {
		this.tipoDatoId = tipoDatoId;
	}

	/**
	 * @return the activo
	 */
	public Boolean getActivo() {
		return activo;
	}

	/**
	 * @param activo the activo to set
	 */
	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	
}
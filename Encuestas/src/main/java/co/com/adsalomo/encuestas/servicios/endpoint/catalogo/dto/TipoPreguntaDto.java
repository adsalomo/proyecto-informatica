package co.com.adsalomo.encuestas.servicios.endpoint.catalogo.dto;

import java.io.Serializable;

/**
 * Dto tipo pregunta
 * @author alopez
 *
 */
public class TipoPreguntaDto implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer tipoPreguntaId;
	private Boolean activo;
	private String nombre;

	public TipoPreguntaDto() {
	}

	/**
	 * @return the tipoPreguntaId
	 */
	public Integer getTipoPreguntaId() {
		return tipoPreguntaId;
	}

	/**
	 * @param tipoPreguntaId the tipoPreguntaId to set
	 */
	public void setTipoPreguntaId(Integer tipoPreguntaId) {
		this.tipoPreguntaId = tipoPreguntaId;
	}

	/**
	 * @return the activo
	 */
	public Boolean getActivo() {
		return activo;
	}

	/**
	 * @param activo the activo to set
	 */
	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	
}
package co.com.adsalomo.encuestas.servicios.endpoint.catalogo.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import co.com.adsalomo.encuestas.servicios.dao.model.Encuesta;
import co.com.adsalomo.encuestas.servicios.endpoint.catalogo.dto.EncuestaDto;

/**
 * Mapper Encuesta
 * 
 * @author alopez
 *
 */
@Mapper
public interface EncuestaMapper {

	EncuestaMapper INSTANCE = Mappers.getMapper(EncuestaMapper.class);

	@Mappings({ 
		@Mapping(source = "usuarioCrea", target = "usuario.usuarioId")
	})
	Encuesta encuestaDtoToEncuesta(EncuestaDto encuestaDto);

	@Mappings({ 
		@Mapping(source = "usuario.usuarioId", target = "usuarioCrea")
	})
	EncuestaDto encuestaToEncuestaDto(Encuesta encuesta);

	List<Encuesta> encuestaDtosToEncuesta(List<EncuestaDto> encuestaDtos);

	List<EncuestaDto> encuestasToEncuestaDtos(List<Encuesta> encuestas);
}

package co.com.adsalomo.encuestas.servicios.endpoint.catalogo.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import co.com.adsalomo.encuestas.servicios.dao.model.Formulario;
import co.com.adsalomo.encuestas.servicios.endpoint.catalogo.dto.FormularioDto;

/**
 * Mapper Formulario
 * 
 * @author alopez
 *
 */
@Mapper
public interface FormularioMapper {

	FormularioMapper INSTANCE = Mappers.getMapper(FormularioMapper.class);

	@Mappings({ 
		@Mapping(source = "tipoPreguntaId", target = "tipoPregunta.tipoPreguntaId")
	})
	Formulario formularioDtoToFormulario(FormularioDto formularioDto);

	@Mappings({ 
		@Mapping(source = "tipoPregunta.tipoPreguntaId", target = "tipoPreguntaId"),
		@Mapping(source = "tipoPregunta.nombre", target = "nombreTipoPregunta")
	})
	FormularioDto formularioToFormularioDto(Formulario formulario);

	List<Formulario> formularioDtosToFormularios(List<FormularioDto> formularioDtos);

	List<FormularioDto> formulariosToFormularioDtos(List<Formulario> formularios);
}

package co.com.adsalomo.encuestas.servicios.endpoint.catalogo.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import co.com.adsalomo.encuestas.servicios.dao.model.Pregunta;
import co.com.adsalomo.encuestas.servicios.endpoint.catalogo.dto.PreguntaDto;

/**
 * Mapper Pregunta
 * 
 * @author alopez
 *
 */
@Mapper
public interface PreguntaMapper {

	PreguntaMapper INSTANCE = Mappers.getMapper(PreguntaMapper.class);

	@Mappings({ 
		@Mapping(source = "formularioId", target = "formulario.formularioId"),
		@Mapping(source = "tipoDatoId", target = "tipoDato.tipoDatoId")
	})
	Pregunta preguntaDtoToPregunta(PreguntaDto preguntaDto);

	@Mappings({ 
		@Mapping(source = "formulario.formularioId", target = "formularioId"),
		@Mapping(source = "tipoDato.tipoDatoId", target = "tipoDatoId")
	})
	PreguntaDto preguntaToPreguntaDto(Pregunta pregunta);

	List<Pregunta> preguntaDtosToPreguntas(List<PreguntaDto> preguntaDtos);

	List<PreguntaDto> preguntasToPreguntaDtos(List<Pregunta> preguntas);
}

package co.com.adsalomo.encuestas.servicios.endpoint.catalogo.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import co.com.adsalomo.encuestas.servicios.dao.model.Respuesta;
import co.com.adsalomo.encuestas.servicios.endpoint.catalogo.dto.RespuestaDto;

@Mapper
public interface RespuestaMapper {

	RespuestaMapper INSTANCE = Mappers.getMapper(RespuestaMapper.class);
	
	@Mappings({ 
		@Mapping(source = "preguntaId", target = "pregunta.preguntaId")
	})
	Respuesta respuestaDtoToRespuesta(RespuestaDto respuestaDto);
	
	@Mappings({ 
		@Mapping(source = "pregunta.preguntaId", target = "preguntaId"),
		@Mapping(source = "pregunta.etiqueta", target = "etiquetaPregunta")
	})
	RespuestaDto respuestaToRespuestaDto(Respuesta respuesta);
	
	List<Respuesta> respuestaDtosToRespuestas(List<RespuestaDto> respuestaDtos);
	
	List<RespuestaDto> respuestasToRespuestaDtos(List<Respuesta> respuestas);
}

package co.com.adsalomo.encuestas.servicios.endpoint.catalogo.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import co.com.adsalomo.encuestas.servicios.dao.model.TipoDato;
import co.com.adsalomo.encuestas.servicios.endpoint.catalogo.dto.TipoDatoDto;

/**
 * Mapper TipoDato
 * @author alopez
 *
 */
@Mapper
public interface TipoDatoMapper {

	TipoDatoMapper INSTANCE = Mappers.getMapper(TipoDatoMapper.class);
	
	TipoDato tipoDatoDtoToTipoDato(TipoDatoDto tipoDatoDto);
	
	TipoDatoDto tipoDatoToTipoDatoDto(TipoDato tipoDato);
	
	List<TipoDato> tipoDatoDtosToTipoDatos(List<TipoDatoDto> tipoDatoDtos);
	
	List<TipoDatoDto> tipoDatosToTipoDatoDtos(List<TipoDato> tipoDatos);
}

package co.com.adsalomo.encuestas.servicios.endpoint.catalogo.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import co.com.adsalomo.encuestas.servicios.dao.model.Usuario;
import co.com.adsalomo.encuestas.servicios.endpoint.catalogo.dto.UsuarioDto;

/**
 * Mapper usuario
 * @author alopez
 *
 */
@Mapper
public interface UsuarioMapper {

	UsuarioMapper INSTANCE = Mappers.getMapper(UsuarioMapper.class);
	
	Usuario usuarioDtoToUsuario(UsuarioDto usuarioDto);
	
	UsuarioDto usuarioToUsuarioDto(Usuario usuario);
	
	List<Usuario> usuarioDtosToUsuarios(List<UsuarioDto> usuarioDtos);
	
	List<UsuarioDto> usuariosToUsuarioDtos(List<Usuario> usuarios);
}

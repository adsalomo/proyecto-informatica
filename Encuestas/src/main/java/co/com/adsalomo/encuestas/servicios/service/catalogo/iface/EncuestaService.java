package co.com.adsalomo.encuestas.servicios.service.catalogo.iface;

import java.util.List;

import co.com.adsalomo.encuestas.servicios.endpoint.catalogo.dto.CatalogoRespuestaDto;
import co.com.adsalomo.encuestas.servicios.endpoint.catalogo.dto.EncuestaDto;

public interface EncuestaService {

	/**
	 * Obtiene todas por activo
	 * @param activo
	 * @return
	 */
	List<EncuestaDto> listarPorActivo(Boolean activo);
	
	/**
	 * Obtiene la encuesta por id
	 * @param encuestaId
	 * @return
	 */
	EncuestaDto listarPorEncuestaId(Integer encuestaId);
	
	/**
	 * Guarda encuesta
	 * @param encuestaDto
	 * @return
	 */
	CatalogoRespuestaDto guardar(EncuestaDto encuestaDto);
	
	/**
	 * Obtiene el resultado de la encuesta
	 * @param encuestaId
	 * @return
	 */
	EncuestaDto obtenerResultados(Integer encuestaId);
}

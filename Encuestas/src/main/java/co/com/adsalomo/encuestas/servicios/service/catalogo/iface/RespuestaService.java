package co.com.adsalomo.encuestas.servicios.service.catalogo.iface;

import java.util.List;

import co.com.adsalomo.encuestas.servicios.endpoint.catalogo.dto.CatalogoRespuestaDto;
import co.com.adsalomo.encuestas.servicios.endpoint.catalogo.dto.RespuestaDto;

public interface RespuestaService {

	CatalogoRespuestaDto guardar(List<RespuestaDto> respuestaDtos);
}

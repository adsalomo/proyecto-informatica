package co.com.adsalomo.encuestas.servicios.service.catalogo.iface;

import java.util.List;

import co.com.adsalomo.encuestas.servicios.endpoint.catalogo.dto.TipoDatoDto;

public interface TipoDatoService {

	/**
	 * Obtiene todos por activo
	 * @param activo
	 * @return
	 */
	List<TipoDatoDto> listarPorActivo(Boolean activo);
}

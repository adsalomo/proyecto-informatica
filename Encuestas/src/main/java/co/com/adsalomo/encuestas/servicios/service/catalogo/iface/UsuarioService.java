package co.com.adsalomo.encuestas.servicios.service.catalogo.iface;

import java.util.List;

import co.com.adsalomo.encuestas.servicios.endpoint.catalogo.dto.UsuarioDto;

/**
 * Servicio Usuario
 * @author alopez
 *
 */
public interface UsuarioService {

	List<UsuarioDto> listarPorActivo(Boolean activo);
}

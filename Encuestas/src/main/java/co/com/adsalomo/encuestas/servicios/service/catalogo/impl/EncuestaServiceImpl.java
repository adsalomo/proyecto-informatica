package co.com.adsalomo.encuestas.servicios.service.catalogo.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.adsalomo.encuestas.servicios.dao.catalogo.iface.EncuestaRepository;
import co.com.adsalomo.encuestas.servicios.dao.catalogo.iface.FormularioRepository;
import co.com.adsalomo.encuestas.servicios.dao.catalogo.iface.PreguntaRepository;
import co.com.adsalomo.encuestas.servicios.dao.catalogo.iface.RespuestaRepository;
import co.com.adsalomo.encuestas.servicios.dao.model.Encuesta;
import co.com.adsalomo.encuestas.servicios.dao.model.Formulario;
import co.com.adsalomo.encuestas.servicios.dao.model.Pregunta;
import co.com.adsalomo.encuestas.servicios.dao.model.Respuesta;
import co.com.adsalomo.encuestas.servicios.dao.model.TipoDato;
import co.com.adsalomo.encuestas.servicios.dao.model.TipoPregunta;
import co.com.adsalomo.encuestas.servicios.dao.model.Usuario;
import co.com.adsalomo.encuestas.servicios.endpoint.catalogo.dto.CatalogoRespuestaDto;
import co.com.adsalomo.encuestas.servicios.endpoint.catalogo.dto.EncuestaDto;
import co.com.adsalomo.encuestas.servicios.endpoint.catalogo.dto.FormularioDto;
import co.com.adsalomo.encuestas.servicios.endpoint.catalogo.dto.PreguntaDto;
import co.com.adsalomo.encuestas.servicios.endpoint.catalogo.dto.RespuestaDto;
import co.com.adsalomo.encuestas.servicios.endpoint.catalogo.mapper.EncuestaMapper;
import co.com.adsalomo.encuestas.servicios.endpoint.catalogo.mapper.FormularioMapper;
import co.com.adsalomo.encuestas.servicios.endpoint.catalogo.mapper.PreguntaMapper;
import co.com.adsalomo.encuestas.servicios.endpoint.catalogo.mapper.RespuestaMapper;
import co.com.adsalomo.encuestas.servicios.service.catalogo.iface.EncuestaService;
import co.com.adsalomo.encuestas.servicios.util.Utils;

@Service
public class EncuestaServiceImpl implements EncuestaService {

	@Autowired
	private EncuestaRepository encuestaRepository;
	@Autowired
	private PreguntaRepository preguntaRepository;
	@Autowired
	private FormularioRepository formularioRepository;
	@Autowired
	private RespuestaRepository respuestaRepository;

	@Override
	public List<EncuestaDto> listarPorActivo(Boolean activo) {
		List<Encuesta> encuestas = encuestaRepository.findByActivo(activo);

		if (encuestas == null || encuestas.size() == 0) {
			return null;
		}

		return EncuestaMapper.INSTANCE.encuestasToEncuestaDtos(encuestas);
	}

	@Override
	public EncuestaDto listarPorEncuestaId(Integer encuestaId) {
		Encuesta encuesta = encuestaRepository.findByEncuestaId(encuestaId);

		if (encuesta == null) {
			return null;
		}

		EncuestaDto encuestaDto = EncuestaMapper.INSTANCE.encuestaToEncuestaDto(encuesta);

		/**
		 * Obtenemos formulario
		 */
		Formulario formulario = formularioRepository.findByEncuestaId(encuestaDto.getEncuestaId());
		FormularioDto formularioDto = FormularioMapper.INSTANCE.formularioToFormularioDto(formulario);

		/**
		 * Obtenemos las preguntas
		 */
		List<Pregunta> preguntas = preguntaRepository.findByFormularioId(formularioDto.getFormularioId());
		List<PreguntaDto> preguntaDtos = PreguntaMapper.INSTANCE.preguntasToPreguntaDtos(preguntas);
		
		for (PreguntaDto preguntaDto : preguntaDtos) {
			/**
			 * Eliminamos los caracteres especiales y los espacios
			 */
			String etiqueta = preguntaDto.getEtiqueta().replaceAll(Utils.CADENA_ELIMINA_ESPACIOS, "");
			etiqueta = etiqueta.replaceAll(Utils.CADENA_ELIMINA_CARACTER_ESPECIAL, "");
			
			preguntaDto.setEtiquetaVal(etiqueta.toLowerCase());
			
			if (!preguntaDto.getTipoDatoId().equals(Utils.TIPO_DATO_ENUMERADO)) {
				continue;
			}
			
			String[] valores = preguntaDto.getValor().split(",");
			List<String> valoresLista = new ArrayList<>();
			for (int i = 0; i < valores.length; i++) {
				valoresLista.add(valores[i].trim());
			}
			preguntaDto.setValores(valoresLista);
		}

		formularioDto.setPreguntas(preguntaDtos);
		encuestaDto.setFormulario(formularioDto);

		return encuestaDto;
	}

	@Override
	@Transactional
	public CatalogoRespuestaDto guardar(EncuestaDto encuestaDto) {
		CatalogoRespuestaDto respuestaDto = new CatalogoRespuestaDto();
		
		Encuesta encuesta = new Encuesta();
		encuesta.setFechaCrea(new Date());
		encuesta.setActivo(true);
		encuesta.setFechaModifica(new Date());
		encuesta.setNombre(encuestaDto.getNombre());
		encuesta.setObjetivo(encuestaDto.getObjetivo());
		encuesta.setUsuario(new Usuario(1));
		encuesta.setUsuarioModifica(1);
		
		encuesta = encuestaRepository.save(encuesta);
		
		Formulario formulario = new Formulario();
		formulario.setEncuesta(encuesta);
		formulario.setFechaFin(null);
		formulario.setFechaInicio(encuestaDto.getFormulario().getFechaInicio());
		formulario.setFechaModifica(new Date());
		formulario.setNombre(encuestaDto.getFormulario().getNombre());
		formulario.setTipoPregunta(new TipoPregunta(Utils.TIPO_PREGUNTA_SIN_DATOS));
		formulario.setUsuarioModifica(1);
		
		formulario = formularioRepository.save(formulario);
		
		for (PreguntaDto preguntaDto : encuestaDto.getFormulario().getPreguntas()) {
			Pregunta pregunta = new Pregunta();
			pregunta.setActivo(true);
			pregunta.setEtiqueta(preguntaDto.getEtiqueta());
			pregunta.setFechaModifica(new Date());
			pregunta.setFormulario(formulario);
			pregunta.setLongitud(preguntaDto.getLongitud());
			pregunta.setRequerido(preguntaDto.getRequerido());
			pregunta.setTipoDato(new TipoDato(preguntaDto.getTipoDatoId()));
			pregunta.setUsuarioModifica(1);
			pregunta.setValor(preguntaDto.getValor());
			
			pregunta = preguntaRepository.save(pregunta);
		}
		
		respuestaDto.setEstado(true);
		respuestaDto.setRespuesta("La encuesta ha sido registrada con éxito.");
		
		return respuestaDto;
	}

	@Override
	public EncuestaDto obtenerResultados(Integer encuestaId) {
		Encuesta encuesta = encuestaRepository.findByEncuestaId(encuestaId);

		if (encuesta == null) {
			return null;
		}

		EncuestaDto encuestaDto = EncuestaMapper.INSTANCE.encuestaToEncuestaDto(encuesta);

		/**
		 * Obtenemos formulario
		 */
		Formulario formulario = formularioRepository.findByEncuestaId(encuestaDto.getEncuestaId());
		FormularioDto formularioDto = FormularioMapper.INSTANCE.formularioToFormularioDto(formulario);

		/**
		 * Obtenemos las preguntas
		 */
		List<Pregunta> preguntas = preguntaRepository.findByFormularioId(formularioDto.getFormularioId());
		List<PreguntaDto> preguntaDtos = PreguntaMapper.INSTANCE.preguntasToPreguntaDtos(preguntas);
		
		for (PreguntaDto preguntaDto : preguntaDtos) {
			List<Respuesta> respuestas = respuestaRepository.findByPreguntaId(preguntaDto.getPreguntaId());
			List<RespuestaDto> respuestaDtos = RespuestaMapper.INSTANCE.respuestasToRespuestaDtos(respuestas);
			preguntaDto.setRespuestas(respuestaDtos);
		}

		formularioDto.setPreguntas(preguntaDtos);
		encuestaDto.setFormulario(formularioDto);

		return encuestaDto;
	}

}

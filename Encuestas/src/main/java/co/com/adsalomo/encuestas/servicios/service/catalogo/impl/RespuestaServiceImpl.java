package co.com.adsalomo.encuestas.servicios.service.catalogo.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.adsalomo.encuestas.servicios.dao.catalogo.iface.RespuestaRepository;
import co.com.adsalomo.encuestas.servicios.dao.model.Pregunta;
import co.com.adsalomo.encuestas.servicios.dao.model.Respuesta;
import co.com.adsalomo.encuestas.servicios.endpoint.catalogo.dto.CatalogoRespuestaDto;
import co.com.adsalomo.encuestas.servicios.endpoint.catalogo.dto.RespuestaDto;
import co.com.adsalomo.encuestas.servicios.service.catalogo.iface.RespuestaService;

@Service
public class RespuestaServiceImpl implements RespuestaService {

	@Autowired private RespuestaRepository respuestaRepository;
	
	@Override
	@Transactional
	public CatalogoRespuestaDto guardar(List<RespuestaDto> respuestaDtos) {
		CatalogoRespuestaDto catalogo = new CatalogoRespuestaDto();
		
		for (RespuestaDto respuestaDto : respuestaDtos) {
			Respuesta respuesta = new Respuesta();
			
			Pregunta pregunta = new Pregunta();
			pregunta.setPreguntaId(respuestaDto.getPreguntaId());
			respuesta.setPregunta(pregunta);
			
			respuesta.setValor(respuestaDto.getValor());
			respuesta.setUsuarioModifica(1);
			respuesta.setFechaProceso(new Date());
			respuestaRepository.save(respuesta);
		}
		
		catalogo.setEstado(true);
		catalogo.setRespuesta("Encuesta guardada");
		
		return catalogo;
	}

}

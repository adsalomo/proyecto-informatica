package co.com.adsalomo.encuestas.servicios.service.catalogo.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.adsalomo.encuestas.servicios.dao.catalogo.iface.TipoDatoRepository;
import co.com.adsalomo.encuestas.servicios.dao.model.TipoDato;
import co.com.adsalomo.encuestas.servicios.endpoint.catalogo.dto.TipoDatoDto;
import co.com.adsalomo.encuestas.servicios.endpoint.catalogo.mapper.TipoDatoMapper;
import co.com.adsalomo.encuestas.servicios.service.catalogo.iface.TipoDatoService;

@Service
public class TipoDatoServiceImpl implements TipoDatoService {

	@Autowired private TipoDatoRepository tipoDatoRepository;
	
	@Override
	public List<TipoDatoDto> listarPorActivo(Boolean activo) {
		List<TipoDato> tipoDatos = tipoDatoRepository.findByActivo(activo);
		
		if (tipoDatos == null || tipoDatos.size() == 0) {
			return null;
		}
		
		return TipoDatoMapper.INSTANCE.tipoDatosToTipoDatoDtos(tipoDatos);
		
	}

}

package co.com.adsalomo.encuestas.servicios.service.catalogo.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.adsalomo.encuestas.servicios.dao.catalogo.iface.UsuarioRepository;
import co.com.adsalomo.encuestas.servicios.dao.model.Usuario;
import co.com.adsalomo.encuestas.servicios.endpoint.catalogo.dto.UsuarioDto;
import co.com.adsalomo.encuestas.servicios.endpoint.catalogo.mapper.UsuarioMapper;
import co.com.adsalomo.encuestas.servicios.service.catalogo.iface.UsuarioService;

/**
 * Implementación servicio usuario
 * @author alopez
 *
 */
@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired private UsuarioRepository usuarioRepository;
	
	@Override
	public List<UsuarioDto> listarPorActivo(Boolean activo) {
		List<Usuario> list = usuarioRepository.findByActivo(activo);
		
		if (list == null || list.size() == 0) {
			return null;
		}
		
		List<UsuarioDto> usuarios = UsuarioMapper.INSTANCE.usuariosToUsuarioDtos(list);
		
		return usuarios;
	}

}

package co.com.adsalomo.encuestas.servicios.util;

public class Utils {
	public final static Integer TIPO_DATO_TEXTO_LIBRE = 1;
	public final static Integer TIPO_DATO_NUMERICO = 2;
	public final static Integer TIPO_DATO_ENUMERADO = 3;
	public final static Integer TIPO_DATO_FECHA = 4;
	
	public final static Integer TIPO_PREGUNTA_ABIERTA = 1;
	public final static Integer TIPO_PREGUNTA_CERRADA = 2;
	public final static Integer TIPO_PREGUNTA_SIN_DATOS = 3;
	
	public final static String CADENA_ELIMINA_ESPACIOS = " ";
	public final static String CADENA_ELIMINA_CARACTER_ESPECIAL = "[-+.^:,?¿]";
}

# Proyecto informatica final - Encuestas web
============================================

+ Encuestas: proyecto java maven. Puede abrise con Netbeans o eclipse
+ encuestas-web: proyecto web. Puede abrirse con Netbeans
+ Informatica.eap: modelo relacional. Puede abrise con enterprise architect
+ Encuestas.sql: sql para la creacion de la base de datos: Postgresql
+ Insert.sql: Insert con los parametros iniciales de la aplicaciòn 

*Configuraciòn base de datos*
Una vez clonado el repostirio, ingresar a la carpeta Encuestas\src\main\resources y abrir el archivo application.properties. 
En este archivo modificar la lineas:

```
spring.datasource.url=jdbc:postgresql://localhost:5432/encuestas
spring.datasource.username=postgres
spring.datasource.password=adrian06
```

Para definir la base de datos a usar (postgresql), usuario y contraseña. 

Creada la base de datos, ejecutamos el archivo encuestas.sql y seguidamente el archivo insert.sql dentro de la base de datos creada en postgresql.

Una vez realizado los pasos anteriores, abrimos desde netbeans o eclipse el proyecto Encuestas y lo ejecutamos, y despues abrimos con netbeans el proyecto
encuestas-web y lo ejecutamos.

(function () {
    'use strict';
    angular.module('app',
            [
                'ui.router',
                'ngResource',
                'ngRoute',
                'ui.bootstrap',
                'ngLoadingSpinner'

            ]);
})();



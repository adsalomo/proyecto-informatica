(function () {
    'use strict';
    angular.module('app').controller('EncuestaCrearCtrl', ['$scope', 'EncuestaService', 'TipoDatoService', 'ModalGuardar',
        function ($scope, EncuestaService, TipoDatoService, ModalGuardar) {

            var self = this;
            this.tipoDato = {dataTipoDato: []};
            this.pregunta = {};
            this.encuesta = { formulario: { preguntas: [] } };

            TipoDatoService.obtenerXActivo({activo: 1}).$promise.then(function (data) {
                self.llenarTipoDatos(data);
            }, function (error) {
                console.log(error);
            });

            this.llenarTipoDatos = function (data) {
                angular.forEach(data, function (value, key) {
                    var tipoDato = {};
                    tipoDato['tipoDato'] = value;
                    tipoDato['tipoDatoId'] = value.tipoDatoId;
                    tipoDato['nombre'] = value.nombre;
                    self.tipoDato.dataTipoDato.push(tipoDato);
                });
            };

            this.agregarPregunta = function () {
                self.encuesta.formulario.preguntas.push(self.pregunta);
                self.pregunta = {};
            };

            this.eliminarPregunta = function (pregunta) {
                var index = self.encuesta.formulario.preguntas.indexOf(pregunta);
                self.encuesta.formulario.preguntas.splice(index, 1);
            };
            
            this.guardar = function () {
                EncuestaService.guardar(self.encuesta).$promise.then(function (data) {
                    var config = {
                        titulo: 'Información',
                        mensaje: data.respuesta,
                        template: 'templates/components/modalOK.html'
                    };
                    new ModalGuardar(config);
                }, function (error) {
                    console.log(error);
                    var config = {
                        titulo: 'Error',
                        mensaje: 'Ocurrio un error al procesar la solicitud',
                        template: 'templates/components/modalOK.html'
                    };
                    new ModalGuardar(config);
                });
            };

        }]);
})();




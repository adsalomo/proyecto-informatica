(function () {
    'use strict';
    angular.module('app').controller('EncuestaListarCtrl', ['$scope', 'EncuestaService',
        function ($scope, EncuestaService) {

            var self = this;
            this.encuesta = {dataEncuesta: []};

            EncuestaService.obtenerXActivo({activo: 1}).$promise.then(function (data) {
                self.cargarDatosEncuesta(data);
            }, function (error) {
                console.log(error);
            });

            this.cargarDatosEncuesta = function (data) {
                angular.forEach(data, function (value, key) {
                    var encuesta = {};
                    encuesta['encuesta'] = value;
                    encuesta['nombre'] = value.nombre;
                    encuesta['objetivo'] = value.objetivo;
                    self.encuesta.dataEncuesta.push(encuesta);
                });
            };


        }]);
})();




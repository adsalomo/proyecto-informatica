(function () {
    'use strict';
    angular.module('app').controller('EncuestaProcesarCtrl', ['$scope', 'EncuestaService', '$routeParams', 'RespuestaService', 'ModalGuardar',
        function ($scope, EncuestaService, $routeParams, RespuestaService, ModalGuardar) {

            var self = this;
            this.encuesta = {dataEncuesta: {}, numeroFilas: [], numeroColumnas: 2};

            EncuestaService.obtenerXEncuestaId({encuestaId: $routeParams.encuestaId}).$promise.then(function (data) {
                self.cargarDatosEncuesta(data);
            }, function (error) {
                console.log(error);
            });

            this.cargarDatosEncuesta = function (data) {
                var encuesta = {};
                encuesta['encuesta'] = data;
                encuesta['nombre'] = data.nombre;
                encuesta['objetivo'] = data.objetivo;
                encuesta['formulario'] = data.formulario;

                var numRow = Math.floor(data.formulario.preguntas.length / self.encuesta.numeroColumnas);
                var resto = (data.formulario.preguntas.length % self.encuesta.numeroColumnas);
                if (resto !== 0) {
                    numRow++;
                }

                for (var i = 0, l = numRow; i < l; i++) {
                    self.encuesta.numeroFilas[i] = i;
                }

                self.encuesta.dataEncuesta = encuesta;
            };

            this.procesar = function () {
                var respuesta = [];
                angular.forEach(this.encuesta.dataEncuesta.formulario.preguntas, function (value, key) {
                    var item = {preguntaId: value.preguntaId, valor: value.nuevoValor};
                    respuesta.push(item);
                });

                RespuestaService.guardar(respuesta).$promise.then(function (data) {
                    var config = {
                        titulo: 'Información',
                        mensaje: data.respuesta,
                        template: 'templates/components/modalOK.html'
                    };
                    new ModalGuardar(config);
                }, function (error) {
                    console.log(error);
                    var config = {
                        titulo: 'Error',
                        mensaje: 'Ocurrio un error al procesar la solicitud',
                        template: 'templates/components/modalOK.html'
                    };
                    new ModalGuardar(config);
                });

            };

        }]);
})();




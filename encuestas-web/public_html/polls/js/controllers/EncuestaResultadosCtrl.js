(function () {
    'use strict';
    angular.module('app').controller('EncuestaResultadosCtrl', ['$scope', 'EncuestaService', '$routeParams',
        function ($scope, EncuestaService, $routeParams) {

            var self = this;
            this.encuesta = {};

            EncuestaService.obtenerResultados({encuestaId: $routeParams.encuestaId}).$promise.then(function (data) {
                self.llenarResultados(data);
            }, function (error) {
                console.log(error);
            });

            this.llenarResultados = function (data) {
                var encuesta = {};
                encuesta['encuesta'] = data;
                encuesta['nombre'] = data.nombre;
                encuesta['objetivo'] = data.objetivo;
                encuesta['formulario'] = data.formulario;
                self.encuesta = encuesta;
            };

        }]);
})();




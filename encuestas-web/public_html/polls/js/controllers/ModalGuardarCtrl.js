(function() {
  'use strict';
  angular.module('app').controller('ModalGuardarCtrl', function(config, $uibModalInstance, $route) {
    var self = this;    

    this.config = config;

    this.close = function() {
      $uibModalInstance.dismiss('close');
      $route.reload();
    };
  });
})();


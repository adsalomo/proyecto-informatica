(function () {
    'use strict';

    angular.module('app').config(['$routeProvider', function ($routeProvider) {
            $routeProvider
                    .when('/',
                            {
                                templateUrl: 'templates/app/encuesta_listar.html',
                                controller: 'EncuestaListarCtrl',
                                controllerAs: 'ctrl'
                            })
                    .when('/encuestaProcesar/:encuestaId',
                            {
                                templateUrl: 'templates/app/encuesta_procesar.html',
                                controller: 'EncuestaProcesarCtrl',
                                controllerAs: 'ctrl'
                            })
                    .when('/encuestaCrear/',
                            {
                                templateUrl: 'templates/app/encuesta_crear.html',
                                controller: 'EncuestaCrearCtrl',
                                controllerAs: 'ctrl'
                            })
                    .when('/encuestaResultados/:encuestaId',
                            {
                                templateUrl: 'templates/app/encuesta_resultados.html',
                                controller: 'EncuestaResultadosCtrl',
                                controllerAs: 'ctrl'
                            })
                    ;
        }]);

})();
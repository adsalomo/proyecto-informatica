(function () {
    'use strict';
    angular.module('app').service('EncuestaService', function ($resource, BaseUrl) {
        return $resource(BaseUrl + '/catalogo/encuesta', {},
                {
                    obtenerXActivo: {
                        url: BaseUrl + '/catalogo/encuesta/:activo',
                        method: 'GET',
                        params: {activo: '@activo'},
                        isArray: true
                    },
                    obtenerXEncuestaId: {
                        url: BaseUrl + '/catalogo/encuesta/obtenerXEncuestaId/:encuestaId',
                        method: 'GET',
                        params: {encuestaId: '@encuestaId'},
                        isArray: false
                    },
                    guardar: {
                        method: 'POST'
                    },
                    obtenerResultados: {
                        url: BaseUrl + '/catalogo/encuesta/obtenerResultados/:encuestaId',
                        method: 'GET',
                        params: {encuestaId: '@encuestaId'},
                        isArray: false
                    }
                });
    });
})();



(function () {
    'use strict';
    angular.module('app').service('RespuestaService', function ($resource, BaseUrl) {
        return $resource(BaseUrl + '/catalogo/respuesta', {}, 
	{
            guardar: {
                method: 'POST'
            }
	});
    });
})();



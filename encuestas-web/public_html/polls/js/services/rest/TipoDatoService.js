(function () {
    'use strict';
    angular.module('app').service('TipoDatoService', function ($resource, BaseUrl) {
        return $resource(BaseUrl + '/catalogo/tipoDato', {}, 
	{
		obtenerXActivo: {
			url: BaseUrl + '/catalogo/tipoDato/:activo', 
			method: 'GET', 
			params: {activo: '@activo'}, 
			isArray: true
		}
	});
    });
})();



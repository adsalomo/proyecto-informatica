(function () {
    'use strict';

    angular.module('app').factory('ModalGuardar', function ($uibModal) {

        function ModalGuardar(config) {
            var templateUrl = config.template;

            $uibModal.open({
                templateUrl: templateUrl,
                controller: 'ModalGuardarCtrl',
                controllerAs: 'ctrl',
                size: 'md',
                resolve: {
                    config: function () {
                        return config;
                    }
                }
            });
        }
        return ModalGuardar;
    });

    angular.module('app').filter('removeSpaces', [function () {
            return function (string) {
                if (!angular.isString(string)) {
                    return string;
                }
                return string.replace(/[\s]/g, '');
            };
        }])
})();


